const app = getApp()

const defaultAvatarUrl = 'https://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRna42FI242Lcia07jQodd2FJGIYQfG0LAJGFxM4FbnQP6yfMxBgJ0F3YRqJCJ1aPAK2dQagdusBZg/0'


Page({
    data: {
        avatarUrl: defaultAvatarUrl,
        theme: wx.getSystemInfoSync().theme,
        openid: "",
    },
    onLoad() {
        // 获取用户的openid
        this.setData({
            openid: wx.getStorageSync('openid')
        })
        wx.onThemeChange((result) => {
            this.setData({
                theme: result.theme
            })
        })
    },


    // 获取输入的标题内容
    sigleInput(res) {
        var nickName = res.detail.value
        console.log("昵称-->", nickName);
        this.setData({
            theme: nickName
        })
    },

    // 确定按钮点击事件处理函数
    onConfirm() {
        const openid = this.data.openid
        const nickName = this.data.theme
        if (!nickName || nickName == "") {
            wx.showToast({
                title: '请输入用户名',
                icon: 'none',
            });
            return;
        }
        // 更新帖子中的发布人昵称
        this.updateArticalInfo(openid, nickName, "");
        // 更新用户的评论昵称
        this.updateCommentInfo(openid, nickName, "");
        // 更新用户的昵称
        this.updateUserInfo(openid, nickName, "");

        var userInfo = wx.getStorageSync('userInfo')
        userInfo.nickName = nickName
        // 将用户信息保存到本地缓存
        wx.setStorageSync('userInfo', userInfo);
        // 保存到全局
        app.globalData.userInfo = userInfo;
    },

    // 头像
    async onChooseAvatar(e) {
        const { avatarUrl } = e.detail
        this.setData({
            avatarUrl,
        })
        const openid = this.data.openid
        const getImageInfoAns = await this.getImageInfo(avatarUrl);
        if (!getImageInfoAns) {
            console.log("获取头像图片信息失败");
            return;
        }
        console.log("获取头像图片信息成功-->", getImageInfoAns);
        const publisherImageUrl = await this.upLoadImage(getImageInfoAns.path); //头像地址

        // 更新帖子中的发布人头像
        const updateAns = this.updateArticalInfo(openid, "", publisherImageUrl);
        // 更新用户的评论
        const updateCommentAns = this.updateCommentInfo(openid, "", publisherImageUrl);
        // 更新用户的头像
        const ans = this.updateUserInfo(openid, "", publisherImageUrl);
        var userInfo = wx.getStorageSync('userInfo')
        userInfo.imageUrl = publisherImageUrl
        // 将用户信息保存到本地缓存
        wx.setStorageSync('userInfo', userInfo);
        // 保存到全局
        app.globalData.userInfo = userInfo;
    },


    // 获取图片信息
    async getImageInfo(imageUrl) {
        try {
            const res = await wx.getImageInfo({
                src: imageUrl,
            });
            return res;
        } catch (err) {
            console.log("获取头像图片信息失败", err);
        }
    },

    // 1 上传图片到云数据库
    // 上传文件(绑定文件名为fileFlag)
    async upLoadImage(filePath) {
        try {
            const res = await wx.cloud.uploadFile({
                cloudPath: filePath.slice(11), //根据图片的临时网址进行截取字符串作为图片名
                filePath: filePath, //图片的临时路径
            });
            console.log("图片上传成功,信息为-->", res)
            return res.fileID; //返回图片信息
        } catch (err) {
            console.log("图片上传失败-->", err)
            // 修改状态，标记任务执行异常
            this.setData({
                state: 1
            })
        }
    },


    // 更新帖子的发布人昵称或者头像
    async updateArticalInfo(openid, publisherName, publisherImageUrl) {
        var info = {};
        if (publisherName == "") {
            info = {
                publisherImageUrl: publisherImageUrl, //图片在数据库中的地址
            }
        } else {
            info = {
                publisherName: publisherName, //用户的微信昵称
            }
        }
        try {
            const res = await wx.cloud.database().collection('article').where({
                _openid: openid
            })
                .update({
                    data: info
                });
            console.log("该用户上传过的文章更新成功-->", res);
            return res;
        } catch (err) {
            console.log("该用户上传过的文章更新失败-->", err);
        }
    },

    // 更新评论的发布人昵称或者头像
    async updateCommentInfo(openid, publisherName, publisherImageUrl) {
        var info = {};
        if (publisherName == "") {
            info = {
                publisherImageUrl: publisherImageUrl, //图片在数据库中的地址
            }
        } else {
            info = {
                publisherName: publisherName, //用户的微信昵称
            }
        }
        try {
            const res = await wx.cloud.database().collection('comment').where({
                _openid: openid
            })
                .update({
                    data: info
                });
            console.log("该用户发布过的评论更新成功-->", res);
            return res;
        } catch (err) {
            console.log("该用户发布过的评论更新失败-->", err);
        }
    },


    // 更新用户昵称和头像数据，user数据表
    async updateUserInfo(openid, publisherName, publisherImageUrl) {
        var info = {};
        if (publisherName == "") {
            info = {
                imageUrl: publisherImageUrl, //图片在数据库中的地址
            }
        } else {
            info = {
                name: publisherName, //用户的微信昵称
            }
        }
        try {
            const res = await wx.cloud.database().collection('user')
                .where({
                    _openid: openid
                })
                .update({
                    data: info
                });
            wx.showToast({
                title: '更新成功',
                duration: 1500
            })
            console.log("用户更新信息成功", res)
            return res;
        } catch (err) {
            console.log("用户信息更新失败", err);
        }
    },
})
